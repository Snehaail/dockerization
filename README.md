# Assignment 2: Dockerizing a Python Application

## Objective
Containerize a Flask or FastAPI application using Docker and Docker Compose.

## Repository URL
[GitLab Repository](https://gitlab.com/Snehaail/dockerization)

## Files
- `Dockerfile`: Docker configuration.
- `docker-compose.yml`: Docker Compose configuration.
- `app.py`: Application code.
- `requirements.txt`: Dependencies.

## Instructions
- Build Docker Image: `docker build -t my-python-project .`
- Run Docker Container: `docker run -p 5003:5000 my-python-project`
- Use Docker Compose: `docker-compose up --build`

##Output
-Application runs on http://98.70.32.170:5000/
